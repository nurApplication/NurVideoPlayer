###更新了2.1.0版本[ijkplayer的封装！！！](https://github.com/JingShan666/ijkPlayerMD2)

#####支持屏幕滑动--滑动时间,亮度,声音,支持全屏-单屏,双击暂停--继续,锁定屏幕，支持HTTP和https。最大的特点是你可以控制声道（单声道播放实现KTV功能）。实现了《单片购买-图片广告》功能

![QzpcVXNlcnNcQWRtaW5pc3RyYXRvclxBcHBEYXRhXFJvYW1pbmdcRGluZ1RhbGtcNDY5MjcyMzY0X3YyXEltYWdlRmlsZXNcMTU4NzAzMjU2NzIxMl9BMjk3QzRFRC02RDA4LTRmMWUtOEVCNS03MDhEQTNFNzUxN0IucG5n.png](https://images.gitee.com/uploads/images/2020/0417/114626_df210548_2001628.png)
![test (1).gif](https://upload-images.jianshu.io/upload_images/6941075-db4e263b9dbb8619.gif?imageMogr2/auto-orient/strip)



###使用

1.不要忘记项目的根build.gradle配置
```
allprojects {
        repositories {
            ...
            maven { url 'https://jitpack.io' }
        }
    }
```
2.使用前需要先在项目中添加OkHttp的依赖库
````
    implementation 'com.gitee.nurApplication:NurVideoPlayer:2.1.9'
````
3.AndroidManifest.xml中
####注意自己的activity里面添加这两行
```
            android:configChanges="orientation|screenSize|keyboardHidden"
            android:screenOrientation="portrait"
```
不要忘记！！！不然全屏无效
例如：
```
<activity
            android:name=".activityName"
            android:configChanges="orientation|screenSize|keyboardHidden"
            android:screenOrientation="portrait"/>
```
[使用最新版本](https://jitpack.io/#com.gitee.nurApplication/NurVideoPlayer)


4.添加网络权限
```
<uses-permission android:name="android.permission.INTERNET"/>
```

##xml
NurVideoView是继承```LinearLayout``的``orientation``是``VERTICAL``
````
    <com.nurmemet.nur.nurvideoplayer.NurVideoView
        android:id="@+id/video_view"
        android:layout_width="match_parent"
        android:layout_height="match_parent">


        <!--播放器下面的UI写在这里-->

    </com.nurmemet.nur.nurvideoplayer.NurVideoView>
```` 
``android:layout_height=``一定要``"match_parent"``
参数xml中只有一个``app:video_view_height``也就是视频播放器的高度,默认值``"match_parent"``

默认样式：
![e81d40b9e196ff1b1a5f12ee77ae4b9.jpg](https://images.gitee.com/uploads/images/2020/0417/114625_37771622_2001628.jpeg)
设置了``app:video_view_height="260dp"``后
![8d5cdff9aa4e1e5e7029692f85afc10.jpg](https://images.gitee.com/uploads/images/2020/0417/114625_0b67ccaa_2001628.jpeg)
也就是说这里的``video_view_height``是播放器的高度
###java
初始化
````
 String url = "http://vfx.mtime.cn/Video/2019/02/04/mp4/190204084208765161.mp4";
        NurVideoView videoView = findViewById(R.id.video_view);
        videoView.setUp(this, url, "This is video title");
        videoView.start();
````
需要在``onPause() & onResume () & onKeyDown() ``中调用
```
    @Override
    public void onBackPressed() {
        if (nurVideoPlayer.getIsFullScreen()) {
            nurVideoPlayer.setChangeScreen(false);
        } else
            super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        nurVideoPlayer.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        nurVideoPlayer.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        nurVideoPlayer.stopPlay();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean b = nurVideoPlayer.onKeyDown(keyCode);
        return b || super.onKeyDown(keyCode, event);
    }
```
#参数 
#### 方法
## 
 return     | 方法|注释|作用
-------- | ---|---|--
``void``| ``setUp(Activity activity, String url, String videoName)``|url视频的地址,videoName|初始化
  ``void``| ``start();``| |开始播放
 ``void`` | ``pause();``||暂停 
  ``void``| ``resume();``| |继续 
``void``| ``stopPlay();``| |禁止播放
``void``|``setMaxADView(View v)``||满（就是播放器的上面）-广告view
``void``|``setSmallADView(View v)``||小广告view,单片购买等等
还有好多方法你自己去看吧。。。。

###listener（监听）
 return     | 方法|注释|作用
-------- | ---|---|--
``void``| ``setOnMediaListener(OnMediaListener mediaListener)``||监听播放进度,更改屏幕（全屏）等
``void``| ``setOnControlClickListener(OnControlClickListener controlClickListener);``| |监听点击事件，返回按钮，麦克风按钮等

#### 
