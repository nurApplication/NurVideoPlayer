package com.nurmemet.nur.nurvideoplayerdemo;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import com.nurmemet.nur.nurvideoplayer.NurVideoView;
import com.nurmemet.nur.nurvideoplayer.listener.OnControlClickListener;

public class MainActivity extends Activity {

    private NurVideoView nurVideoPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        String url = "https://stream7.iqilu.com/10339/upload_transcode/202002/09/20200209105011F0zPoYzHry.mp4";
        String url = "http://vfx.mtime.cn/Video/2019/02/04/mp4/190204084208765161.mp4";

        nurVideoPlayer = findViewById(R.id.videoView);
        nurVideoPlayer.setUp(this, url, "This is video title");
    }

    @Override
    public void onBackPressed() {
        if (nurVideoPlayer.getIsFullScreen()) {
            nurVideoPlayer.setChangeScreen(false);
        } else
            super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        nurVideoPlayer.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        nurVideoPlayer.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        nurVideoPlayer.stopPlay();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean b = nurVideoPlayer.onKeyDown(keyCode);
        return b || super.onKeyDown(keyCode, event);
    }

}
